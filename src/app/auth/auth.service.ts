import {Injectable} from '@angular/core';
import {UserModel} from './user/user.model';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private accessToken: string;
  private user: UserModel;
  private AUTH_TOKENS_STRING = 'authToken';
  private USER_DETAILS_STING = 'userDetails';

  constructor(private cookieService: CookieService) {
    this.user = new UserModel();
    this.getCookie();
  }

  getCookie() {
    const accessToken: string = this.cookieService.get(this.AUTH_TOKENS_STRING);
    const details: string = this.cookieService.get(this.USER_DETAILS_STING);
    if (!(this.isNullOrUndefined(accessToken) || this.isNullOrUndefined(details))) {
      this.accessToken = accessToken;
      this.user = JSON.parse(details);
    }
  }

  setUserData(response): void {
    this.accessToken = response.accessToken;
    this.user.firstName = response.firstName;
    this.user.lastName = response.lastName;
    this.user._id = response._id;
    this.setUserRole(response);
    this.cookieService.set(this.AUTH_TOKENS_STRING, this.accessToken);
    this.cookieService.set(this.USER_DETAILS_STING, JSON.stringify(this.user));
  }

  getUser(): UserModel {
    return this.user;
  }


  getToken(): string {
    return this.accessToken || '';
  }

  isAuthorize(): boolean {
    return !!this.accessToken && !!this.user;
  }

  clear(): void {
    this.accessToken = undefined;
    this.user = new UserModel();
    this.cookieService.delete(this.AUTH_TOKENS_STRING);
    this.cookieService.delete(this.USER_DETAILS_STING);
  }

  isNullOrUndefined(item) {
    return item == null || item === undefined || !item;
  }

  isAdmin() {
    return this.user && this.user.role === 'ADMIN';
  }

  private setUserRole(response: any) {
    this.user.role = response.isAdmin ? 'ADMIN' : 'USER';
  }
}
