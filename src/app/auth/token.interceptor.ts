import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {MatSnackBarComponent} from '../common/components/mat-snack-bar/mat-snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService, private snackBar: MatSnackBarComponent) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.auth.getToken();

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request).pipe(
      tap(event => event,
        response => {
          this.snackBar.openSnackBar(this.getErrorMessage(response), 'Zamknij', 'red-snackbar');
        })
    );
  }

  private getErrorMessage(response): string {
    if (response.status === 404) {
      return 'Błąd 404 rządania';
    }

    return (response.error &&
      response.error[0] &&
      response.error[0].constraints &&
      Object.keys(response.error[0].constraints)[0] &&
      response.error[0].constraints[Object.keys(response.error[0].constraints)[0]]) ||
      (response.error &&
      response.error &&
      response.error.error) ||
      '';
  }
}
