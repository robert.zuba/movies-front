import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';
import {RegisterUser} from './user/register.model';
import {catchError, map} from 'rxjs/operators';
import {throwError, Observable} from 'rxjs';
import {tap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private AUTHORIZATION = 'Authorization';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  register(registerUser: RegisterUser) {
    return this.http.post(environment.SERVER_ADDRESS + '/auth/register', registerUser).pipe(
      map(response => response),
      catchError(this.handleError)
    );
  }

  login(email: string, password: string) {
    return this.http.post(environment.SERVER_ADDRESS + '/auth/login', {email, password});
  }

  updateUserDetails(): Observable<any> {
    return this.http.get(environment.SERVER_ADDRESS + '/user/details')
      .pipe(
        tap(userDetails => this.authService.getUser()),
      );
  }

  isAuthorized() {
    return this.authService.isAuthorize();
  }

  isAdmin(): boolean {
    return this.authService.isAdmin();
  }

  getUsername(): string {
    return `${this.authService.getUser().firstName} ${this.authService.getUser().lastName}`;
  }

  getUserId(): string {
    return this.authService.getUser()._id;
  }

  logout() {
    this.authService.clear();
  }

  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return throwError(errMsg);
  }
}
