export class UserModel {
  constructor(public firstName?: string,
              public lastName?: string,
              public email?: string,
              public _id?: string,
              public role?: string) {
  }
}
