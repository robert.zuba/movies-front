import {Component, ViewChild} from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {RegisterUser} from '../user/register.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  @ViewChild('registerForm') private registerForm: NgForm;

  public user = {firstName: '', lastName: '', email: '', password: ''};

  constructor(private userService: UserService, private router: Router) {
  }

  register(): void {
    if (this.registerForm.invalid) {
      return;
    }

    const user = new RegisterUser(
      this.user.firstName,
      this.user.lastName,
      this.user.email,
      this.user.password,
    );

    this.userService.register(user).subscribe(
      () => this.router.navigateByUrl('/login')
    );
  }

}
