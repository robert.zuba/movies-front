import {Component} from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {filter, mergeMap, tap} from 'rxjs/internal/operators';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private userService: UserService, private router: Router, private authService: AuthService) {
  }

  login(email, password): void {
    this.userService.login(email, password)
      .pipe(
        filter(response => !!response),
        tap(response => this.authService.setUserData(response)),
        mergeMap(() => this.router.navigateByUrl('/mainpage')),
      ).subscribe();
  }
}
