export class Review {
  constructor(  public _id?: string,
                public rating?: string,
                public description?: string,
                public author?: object) {
  }
}
