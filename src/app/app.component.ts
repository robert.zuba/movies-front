import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from './auth/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public username;
  constructor(private router: Router, private userService: UserService) {
    if (userService.isAuthorized()) {
      // this.navigate('/applications')
    }
  }

  isAuthorized(): boolean {
    this.setUsernameIfAuthorized();
    return this.userService.isAuthorized();
  }

  isAdmin(): boolean {
    return this.userService.isAdmin();
  }

  logout(): void {
    this.userService.logout();
    this.router.navigateByUrl('/mainpage');
  }

  navigate(route: string): void {
    this.router.navigate([route]);
  }

  private setUsernameIfAuthorized (): void {
    if (this.userService.isAuthorized()) {
      this.username = this.userService.getUsername();
    }
  }
}
