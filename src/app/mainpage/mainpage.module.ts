import {NgModule} from '@angular/core';
import {CommonModule} from '../common/common.module';
import {MainpageComponent} from './mainpage.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MainpageComponent
  ],
  entryComponents: [
    MainpageComponent
  ],
  providers: []
})
export class MainpageModule {
}
