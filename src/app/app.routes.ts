import {Routes} from '@angular/router';
import {LoginComponent} from './auth/login/login.component';
import {MainpageComponent} from './mainpage/mainpage.component';
import {RegisterComponent} from './auth/register/register.component';
import {AddMovieComponent} from './movies/add/add-movie.component';
import {MoviesComponent} from './movies/list/movies.component';
import {MovieDetailsComponent} from './movies/details/movie-details.component';

export const rootRouterConfig: Routes = [
  {path: '',  component: MainpageComponent},
  {path: 'mainpage', component: MainpageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'movies', component: MoviesComponent},
  {path: 'movies/add', component: AddMovieComponent},
  {path: 'movies/details/:id', component: MovieDetailsComponent}
];
