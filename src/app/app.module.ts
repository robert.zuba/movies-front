import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {rootRouterConfig} from './app.routes';
import {AuthModule} from './auth/auth.module';
import {CommonModule} from './common/common.module';
import {MainpageModule} from './mainpage/mainpage.module';
import {AuthService} from './auth/auth.service';
import {UserService} from './auth/user.service';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {TokenInterceptor} from './auth/token.interceptor';
import {MoviesModule} from './movies/movies.module';
import {MatSnackBarComponent} from './common/components/mat-snack-bar/mat-snack-bar.component';
import {MatSnackBarModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    MatSnackBarComponent
  ],
  imports: [
    RouterModule.forRoot(rootRouterConfig),
    CommonModule,
    AuthModule,
    MainpageModule,
    MoviesModule,
    MatSnackBarModule
  ],
  providers: [
    UserService,
    AuthService,
    CookieService,
    MatSnackBarComponent,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
