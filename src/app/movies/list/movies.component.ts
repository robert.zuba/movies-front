import {Component} from '@angular/core';
import {MoviesService} from '../movies.service';
import {Router} from '@angular/router';
import {UserService} from '../../auth/user.service';
import {Movie} from '../movie.model';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent {
  public movies: Movie[] = [];

  constructor(private moviesService: MoviesService, private userService: UserService, private router: Router) {
    this.getCampaigns();
  }

  getCampaigns(): void {
    this.moviesService.getAll().subscribe(movies => {
      this.movies = movies;
    });
  }

  showMovieDetails(id): void {
    this.router.navigate([`/movies/details/${id}`]);
  }

  isAdmin(): boolean {
    return this.userService.isAdmin();
  }

  addMovie (): void {
    this.router.navigate(['/movies/add']);
  }

  search (name: string): void {
    this.moviesService.search(name).subscribe(
      movies => this.movies = movies
    );
  }
}
