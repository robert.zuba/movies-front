import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {Router} from '@angular/router';
import {Movie} from '../movie.model';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html'
})
export class AddMovieComponent implements OnInit {
  private movie: Movie;

  constructor(private moviesService: MoviesService, private router: Router) {
    this.movie = new Movie();
  }

  public ngOnInit(): void {

  }

  public add() {
    this.moviesService.add(this.movie)
      .subscribe(() => {
        this.router.navigate(['/mainpage']);
      });
  }

  public addGenre (genre: string): void {
    if (genre) {
      this.movie.genresList.push(genre);
    }
  }

  public removeGenre (genre: string): void {
    this.movie.genresList = this.movie.genresList.filter(gen => gen !== genre);
  }

  public addCast (c: string): void {
    if (c) {
      this.movie.cast.push(c);
    }
  }

  public removeCast (c: string): void {
    this.movie.cast = this.movie.cast.filter(ca => ca !== c);
  }
}

