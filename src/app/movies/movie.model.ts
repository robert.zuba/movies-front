import {Review} from '../common/models/review.model';

export class Movie {
  constructor(  public _id?: string,
                public title?: string,
                public imageURL?: string,
                public description?: string,
                public favoritesCount?: any,
                public genresList?: string[],
                public releaseDate?: string,
                public budget?: string,
                public revenue?: string,
                public director?: string,
                public cast?: string[],
                public isCurrentUserFavorite?: boolean,
                public reviews?: Review[]) {
    this.genresList = [];
    this.cast = [];
  }
}
