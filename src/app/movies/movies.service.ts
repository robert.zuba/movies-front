import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from './movie.model';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private ENDPOINT_ADDRESS = environment.SERVER_ADDRESS + '/movies';

  constructor(private http: HttpClient) {
  }

  add(movie: Movie) {
    return this.http.post(this.ENDPOINT_ADDRESS, movie);
  }
  getAll (): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.ENDPOINT_ADDRESS).pipe(
      map(response => response)
    );
  }

  getOne (id): Observable<Movie> {
    return this.http.get<Movie>(`${this.ENDPOINT_ADDRESS}/${id}`).pipe(
      map(response => response)
    );
  }
  checkFavorite (id): Observable<any> {
    return this.http.get(`${environment.SERVER_ADDRESS}/users/favorite/${id}`).pipe(
      map(response => response)
    );
  }
  delete (id): Observable<any> {
    return this.http.delete(`${this.ENDPOINT_ADDRESS}/${id}`).pipe(
      map(response => response)
    );
  }

  like (movieId): Observable<any> {
    return this.http.post(`${environment.SERVER_ADDRESS}/users/favorite/${movieId}`, {}).pipe(
      map(response => response)
    );
  }

  dislike (movieId): Observable<any> {
    return this.http.post(`${environment.SERVER_ADDRESS}/users/unfavorite/${movieId}`, {}).pipe(
      map(response => response)
    );
  }

  search(title: string): Observable<Movie[]> {
    return this.http.post<Movie[]>(`${this.ENDPOINT_ADDRESS}/find`, { title: { '$regex': title }}).pipe(
      map(response => response)
    );
  }
}
