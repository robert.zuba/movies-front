import {NgModule} from '@angular/core';
import {CommonModule} from '../common/common.module';
import {MoviesComponent} from './list/movies.component';
import {AddMovieComponent} from './add/add-movie.component';
import {MovieDetailsComponent} from './details/movie-details.component';
import {MatIconModule, MatTableModule, MatTooltipModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material/paginator';
import {RatingModule} from 'ngx-rating';


@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    RatingModule
  ],
  declarations: [
    MoviesComponent,
    AddMovieComponent,
    MovieDetailsComponent
  ],
  providers: []
})
export class MoviesModule {
}
