import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from './movie.model';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Review} from '../common/models/review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  private ENDPOINT_ADDRESS = environment.SERVER_ADDRESS + '/reviews';

  constructor(private http: HttpClient) {
  }

  add(movieId: string, review: Review) {
    return this.http.post(`${this.ENDPOINT_ADDRESS}/${movieId}`, review);
  }
  getAll (): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.ENDPOINT_ADDRESS).pipe(
      map(response => response)
    );
  }

  getOne (id): Observable<Movie> {
    return this.http.get<Movie>(`${this.ENDPOINT_ADDRESS}/${id}`).pipe(
      map(response => response)
    );
  }
  delete (id): Observable<any> {
    return this.http.delete(`${this.ENDPOINT_ADDRESS}/${id}`);
  }
}
