import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../movies.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../auth/user.service';
import {Movie} from '../movie.model';
import {Review} from '../../common/models/review.model';
import {ReviewsService} from '../reviews.service';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']

})
export class MovieDetailsComponent implements OnInit {
  private movie: Movie;
  private review: Review;

  constructor(private moviesService: MoviesService,
              private reviewsService: ReviewsService,
              private route: ActivatedRoute,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    this.movie = new Movie();
    this.review = new Review();
    this.route.params.subscribe(params => {
      this.getMovie(params['id']);
    });
  }

  public getMovie(id: string) {
    forkJoin([this.moviesService.getOne(id), this.moviesService.checkFavorite(id)])
      .subscribe(result => {
        this.movie = result[0];
        this.movie.isCurrentUserFavorite = result[1].favorite;
      });
  }

  deleteMovie(): void {
    this.moviesService.delete(this.movie._id).subscribe(
      () => this.router.navigate(['/movies'])
    );
  }

  addReview(): void {
    this.reviewsService.add(this.movie._id, this.review).subscribe(() => {
      this.review = new Review();
      this.getMovie(this.movie._id);
    });
  }

  isAdmin (): boolean {
    return this.userService.isAdmin();
  }

  isAuthorized (): boolean {
   return this.userService.isAuthorized();
  }

  like () {
    this.moviesService.like(this.movie._id).subscribe(() => this.getMovie(this.movie._id));
  }
  dislike () {
    this.moviesService.dislike(this.movie._id).subscribe(() => this.getMovie(this.movie._id));
  }

  getUserId (): string {
    return this.userService.getUserId();
  }

  removeReview(review: Review) {
    this.reviewsService.delete(review._id).subscribe(() => {
      this.getMovie(this.movie._id);
    });
  }
}
